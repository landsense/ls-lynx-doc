# Changelog
Todas as alterações na API estão documentadas nesse arquivo.

A documentação de cada versão da API se encontra no repositório https://bitbucket.org/landsense/ls-lynx-doc/src

Demonstrações de uso da API estão no repositório https://bitbucket.org/landsense/ls-lynx-samples/src



## [Versão em preparação]

### Inclusões

- Módulo "devices" criado, que permitirá a integração com beacons. A primeura integração está sendo feita com beacons DWM1001-DEV da Decawave usando interface BLE.

### Alterações

### Exclusões



## [0.3.2-beta] - 2019-01-16

### Inclusões
- Comando para apagar arquivos gráficos: ```"set_graphics":"delete"```

### Alterações
- Bug resolvido: o comando ```"get_floorplan":"list"``` não estava filtrando por nome de arquivo gráfico

### Exclusões



## [0.3.1-beta] - 2018-10-21

### Inclusões
- Comandos para gerenciamento de floorplans: "create", "delete", "binding",  
- Comando para criação de floorplans: ```"set_floorplan":"create"```
- Comando para apagar floorplans: ```"set_floorplan":"delete"```
- Comando para associar âncoras a floorplans: ```"set_floorplan":"bind"```
- Comando para desassociar âncoras a floorplans: ```"set_floorplan":"unbind"```

### Alterações
- Texto de introdução escrito em língua portuguesa no arquivo de documentação JSON (json-v0.3.1.beta.md)

### Exclusões



## [0.3.0-beta] - 2018-10-18

### Inclusões
- Comando para obter informações do sistema: ```"get_info": info```
- Comando para definir modo de operação: ```"set_mode": mode```
- Comando para obter lista de beacons: ```"get_beacon":"list"```
- Comando para obter posição de beacons em 3D: ```"get_beacon":"position_xyz"```
- Comando para obter posição de beacons no espaço de plantas-baixas: ```"get_beacon":"position_floorplan"```
- Comando para obter lista de floorplans: ```"get_floorplan":"list"```
- Comando para obter lista de arquivos gráficos: ```"get_graphics":"list"```
- Comando para fazer download de arquivos gráficos: ```"get_graphics":"file"```
- Comando para fazer upload de arquivos gráficos: ```"set_graphics":"file"```

### Alterações

### Exclusões

