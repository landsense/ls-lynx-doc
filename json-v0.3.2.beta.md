# Protocolo JSON usando para comunicação com o servidor RTLS LS-LYNX utilizando socket TCP/IP (release v0.3.2.beta)

## 1. Introdução

O servidor LS-LYNX realiza mapeamento e localização de beacons, manutenção e configuração do sistema, concentrando a interface com o mundo externo.
Atualmente a interface com o mundo externo é por meio de socket TCP/IP, que é um padrão industrial.

O servidor LS-LYNX permanece continuamente monitorando as portas de comunicação TCP/IP. Uma vez uma conexão é estabelecida com um cliente, o servidor processa requisições de acordo com o protocolo JSON descrito nesse documento. Se, para uma determinada conexão não houver requisições por mais de 5s, o servidor fecha a conexão. Isso permite que o servidor não permaneça bloqueado aguardando comandos por uma determinada porta em caso de perda de conexão. O cliente pode realizar uma conexão por requsição, ou permanecer conectado fazendo requisições continuamente até desconectar ou haver perda de conexão.

O servidor opera em dois modos concorrentes: o modo "real" e o modo "simulation". No modo "real" o LS-LYNX opera com beacons reais conectadas a sua rede de beacons. No modo "simulation", LS-LYNX simula uma rede em que os beacons se encontram em determinada configuração espacial. Os estados de ambos os modos são continuamente atualizados pelo servidor, mas os comandos JSON se aplicam apenas ao modo corrente. O modo corrente é selecionado pelo cliente usando o comando ```set_mode```. 

A qualquer momento o cliente mode mudar o modo. Uma vez o modo alterado, os comandos JSON seguintes se aplicam àquele modo. Por exemplo, uma chamada a ```get_beacon``` retorna as informações da rede de beacons reais (modo "real") ou da rede de beacons simulados (modo "simulated"). 

Comandos ```get_info``` permite o cliente acessar variáveis internas do sistema, cujos valores são específicos do modo que está ativo. 

Beacons assumem um dos três papéis seguintes: "none", "tag" e "anchor". Os papéis (ou _roles_, do inglês) são estabelecidos durante implantação e configuração do sistema, havendo comandos específicos para isso. Quando um beacon é identificado pela primeira vez pelo sistema, seu papel é "none". O cliente pode definir seu papel como sendo "anchor" se o beacon for usado como referência para mapeamento do sistema, ou âncora. Beacons âncoras são estáticos, devendo ser implantados em locais fixos e não devem ser movidos com frequencia. A movimentação de âncoras pode ter impacto no posicionamento em plantas-baixas (floorplans, do inglês) ou no posicionamento 3D, nesse último caso se alguma regra espacial for violada. Nessa versão regras espaciais não estão implementadas, mas o serão em versões futuras. Um beacon de papel "tag" pode ser movida livremente.

Plantas-baixas são ambientes delimitados por arquivos gráficos, que podem ser transferidos entre o servidor LS-LYNX e o cliente. Gerenciamento de plantas-baixas é uma tarefa mais complexa que geralmente é feita durante implantação ou configuração do sistema. Para criar uma planta-baixa, o cliente deve transferir para o servidor o arquivo gráfico correspondente, se já não estiver no repositório do servidor. Um mesmo arquivo gráfico pode ser usado por mais de uma planta-baixa, o que permite configurar ambientes similares sem ter que transferir um arquivo gráfico para cada ambiente. Isso otimiza a implantação da solução LS-LYNX em prédios de vários andares similares. Ainda, cada planta-baixa deve ter âncoras vinculadas. Essa vinculação permite a determinação da posição das tags dentro do espaço delimitado pelos arquivos gráficos. Isso porque, como o sistema LS-LYNX mantém as coordenadas 3D de cada tag em tempo-real, suas coordenadas relativas a uma imagem representada pelo arquivo gráfico de uma planta-baixa são obtidas por uma transformação de coordenadas. Para determinar tal transformação, é preciso vincular âncoras a cada planta-baixa.

O sistema LS-LYNX também provê temporização para cada beacon, que pode ser útil caso o cliente implemente alguma técnica de rastreamento. A referência da base de tempo é o instante em que o sistema RTLS foi inicializado. Sempre que as coordenadas de um beacon são atualizadas, um _timestamp_ é associado a ele. O _timestamp_ é um inteiro que corresponde ao número de milisegundos decorridos desde a inicialização do sistema RTLS. Como o _timestamp_ é armazenado em uma variável inteira e sem sinal de 64 bits, seu valor por ir 0 a (2^64)-1, que corresponde de 0 ms a 584942417,355072 anos.  

No modo "simulation", alguns beacons estão pré-configurados como âncoras. Eles estão igualmente espaçados no plano XY em um quadrado de 100m de lado, 20m uns dos outros em uma matriz de 5 x 5 âncoras, e uma altura 1m. Para a montagem de uma planta-baixa essa é a única informação que o usuário pode dispor por meio do protocolo, cabendo a ele vincular as âncoras que cobrem o arquivo gráfico da planta-baixa. 50 tags se deslocam dentro do espaço do quadrado de 100m de lado no plano XY segundo um modelo similar ao movimento humano. A altura das tags varia também. Cabe ao usuário carregar o arquivo gráfico no sistema, criar a planta-baixa e vincular pelo menos três âncoras (quanto mais, maior a precisão). A partir de então o sistema pode passar a fornecer posicionamento na área de cobertura da planta-baixa, além do posicionamento 3D.

A seguir, a mensagens do protocolo JSON são descritas. O protocolo foi desenhado de forma a se dispor de um número reduzido de comandos, mas poderosos e intuitivos.

## 2. System information messages

### 2.1 get_info

#### Message: ```{"get_info": info}``` ####

    info is a string which can be "mode" or "status"

#### Response: ```{info: resp}``` ####

    resp is according to requested info.
    If info is "status", resp is systems status, as below:
        "tracking": System operating normaly, tracking tags
        "mapping" : System is performing mapping. In this case, no tags can be tracked. Anchors positioning is being performed.
    If info is "mode", resp is system mode, as below:
        "simulation": Simulation mode
        "real": Real mode.

## 3. Operating modes

### 3.1 set_mode

      This command allows the client to set the operating mode of LS-LYNX. 

#### Message: ```{"set_mode": mode}``` ####

      mode is a string, as below:
        "simulation": Simulation mode
        "real": Real mode.

#### Response: ```{"mode": mode}``` ####

      mode is as below:
        "simulation": Simulation mode
        "real": Real mode.

## 4. Beacons

### 4.1 Listing

      This command allows the client to receive the list of beacons according to the selected filters.

#### Message: ```{"get_beacon": "list", "status" : status, "role": role, "id": [id1, id2, ..., idN]}``` ####

      This command may have filters. These filters are name/value pairs after the first command pair. Beacon information sent as response are only of beacons satisfying all filters. Filters are optional. If no filter is defined in the message, the requested information of all beacons is sent to the client.
      The defined filters are:
        "status" (optional), the response of the request will me composed of all beacons with a given status, whose value may be "active" or "inactive".
        "role" (optional), the response of the request will me composed of all beacons with a given role, whose value may be "tag", "anchor" or "none".
        "id" (optional), is a filter specifying the IDs of the beacons. It can be an array (for more than one ID) or a number (for an specific ID).


#### Response: ```{"list": [{"id":id1,"role",role1,"status",status1},...,{"id":idN,"role",roleN,"status",statusN}]}``` ####

      The response is organized as an array of JSON objects. For the i-th beacons satisfying the filters, an array objects is presented, where:
        id1, ..., idN: ID's of each beacon satysfying the filters
        role1, ..., roleN: status of each beacon satysfying the filters
        status1, ..., statusN: status of each beacon satysfying the filters
    
      If there is no beacon in the system satisfying the filters, the response is {"list" : [null]}


### 4.2 3D Position at XYZ space

      This command allows the client to receive the 3D positioning of beacons, which may be anchors or tags, according to the selected filters. Only active beacons have their positions computed. 

#### Message: ```{"get_beacon": "position_xyz", "status" : status, "role": role, "id": [id1, id2, ..., idN]}``` ####

      This command may have filters. These filters are name/value pairs after the first command pair. Beacon information sent as response are only of beacons satisfying all filters. Filters are optional. If no filter is defined in the message, the requested information of all beacons is sent to the client.
      The defined filters are:
        "status" (optional), the response of the request will me composed of all beacons with a given status, whose value may be "active" or "inactive".
        "role" (optional), the response of the request will me composed of all beacons with a given role, whose value may be "tag", "anchor" or "none".
        "id" (optional), is a filter specifying the IDs of the beacons. It can be an array (for more than one ID) or a number (for an specific ID).


#### Response: ```{"position_xyz": [{"id":id1,"x":x1,"y":y1,"z":z1,"sx":sx1,"sy":sy1,"sz":sz1,"ts",ts1},...,{"id":id1,"x":x1,"y":y1,"z":z1,"sx":sx1,"sy":sy1,"sz":sz1,"ts",tsN}]}``` ####

      The response is organized as an array of JSON objects. For the i-th beacons satisfying the filters, an array objects is presented, where:
        id1, ..., idN: ID's of each beacon satysfying the filters
        status1, ..., statusN: status of each beacon satysfying the filters
        (x1,y1,z1),..., (xN,yN,zN): coordinates (x,y,z) of the beacon.
        (sx1,sy1,sz1),..., (sxN,syN,szN): 95% confidence interval, centered at x1, y1 and z1 coordinates. For instance, for beacon with id3 there is a 95% confidence that the beacon is inside the region delimited by ([x3-sx3,x3+sx3],[y3-sy3,y3+sy3],[y3-sy3,y3+sy3]) in 3D space.
        ts1,..., tsN: system timestamp of last update of beacon position.
    
      If there is no beacon in the system satisfying the filters, the response is {"position_xyz" : [null]}


### 4.3 Position within floorplan coverage area

      This command allows the client to receive the positioning of beacons which are in floorplan coverage area, which may be anchors or tags, according to the selected filters. It should be pointed out that if some beacon is outside the coverage area of a floorplan, it will be not included in the response. Only active beacons have their positions computed on (also) active floorplans.

#### Message: ```{"get_beacon": "position_floorplan", "status" : status, "role": role, "id": [id1, id2, ..., idN], "fp_id": [fp_id1,...,fp_idN]}``` ####

      This command may have filters. These filters are name/value pairs after the first command pair. Beacon information sent as response are only of beacons satisfying all filters. Filters are optional. If no filter is defined in the message, the requested information of all beacons is sent to the client.
      The defined filters are:
        "status" (optional), the response of the request will me composed of all beacons with a given status, whose value may be "active" or "inactive".
        "role" (optional), the response of the request will me composed of all beacons with a given role, whose value may be "tag", "anchor" or "none".
        "id" (optional), is a filter specifying the IDs of the beacons. It can be an array (for more than one ID) or a number (for an specific ID).
        "floorplan" (optional), is a filter specifying the IDs of the floorplans. It can be an array (for more than one ID) or a number (for an specific ID).


#### Response: ```{"position_floorplan": [{"id":id1,"fp_id":fp_id1,"r":r1,"c":c1,"sr":sr1,"sc":sc1,"ts",ts1},...,{"id":idN,"fp_id":fp_idN,"r":rN,"c":cN,"sr":srN,"sc":scN,"ts",tsN}]}``` ####

      The response is organized as an array of JSON objects. For the i-th beacons satisfying the filters, an array objects is presented, where:
        id1, ..., idN: ID's of each beacon satysfying the filters
        fp_id1, ..., fp_idN: ID's of the corresponding floorplan on which the beacon is in the coverage area.
        (r1,c1),..., (rN,cN): coordinates (row,column) of the beacon in the corresponding floorplan.
        (sr1,sc1),..., (srN,scN): 95% confidence interval, centered at r1 and c1 coordinates. For instance, for beacon with id3 there is a 95% confidence that the beacon is inside the region delimited by ([r3-sr3,r3+sr3],[c3-sc3,c3+sc3]) in floorplan pixels space.
        ts1,..., tsN: system timestamp of last update of beacon position.
    
      It should be noted that a beacon may be in the coverage area of more than one floorplan. Thus, th ID of such a beacon will appear in more than one object in the array.
    
      If there is no beacon in the system satisfying the filters, the response is {"position_floorplan" : [null]}

## 5. Floor plans 

      Each floorplan is composed os a graphics file in formats JPEG, PNG, BMP or TIFF. Floorplans are uploaded to or downloaded from the server using messages. The floorplan file name is specified at uploading message. Uploaded floorplan files are stored in a local repository. 
    
      A floorplan become active when it is associated to beacons defined as anchors. Such floorplans may have beacons being tracked in their coverability area. When there is no anchor associated to a floorplan, its status is inactive. Anchor to floorplan association is refered to as a binding process. Only successfuly binded floorplan and anchors allow calculatin of tag position in a floorplan area. 

### 5.1 Floor plans listing

#### Message: ```{"get_floorplan": "list", "status" : status, "filename": filename, "fp_id" : [fp_id1,...,fp_idN] }``` ####

      This command gathers informations on all floorplan files uploaded at LS-LYNX server.
    
      This command may have filters. These filters are name/value pairs after the first command pair. Beacon information sent as response are only of beacons satisfying all filters. Filters are optional. If no filter is defined in the message, the requested information of all beacons is sent to the client.
      The defined filters are:
        "status" (optional), the response of the request will me composed of all floorplans with a given status, whose value may be "active" or "inactive".
        "filename" (optional), the response of the request will me composed of the floorplan with a given filename.
        "fp_id" (optional), is a filter specifying the IDs of the floorplans. It can be an array (for more than one ID) or a number (for an specific ID).
    
      If there is no floorplan in the system satisfying the filters, the response is {"list" : [null]}

#### Response: ```{"list" : [{"fp_id":fp_id1, "status": status1, "filename": filename1, "anchors":[{"id":id11,"row",r11,"col",col11},...,{"id":id1M,"row",r1M,"col",col1M}], "binding": {"status", binding_status1}}, ...., {"fp_id":fp_idN, "status": statusN, "filename": filenameN, "anchors":[{"id":idN1,"row",rN1,"col",colN1},...,{"id":idNM,"row",rNM,"col",colNM}], "binding": {"status", binding_statusN}}]}``` ####

      The response is organized as an array of JSON objects. For the i-th floorplan satisfying the filters, an array objects is presented, where:
        fp_id1, ..., fp_idN: ID's of each floorplan satysfying the filters
        status1, ..., statusN: status of each floorplan satysfying the filters 
        filename1, ..., filenameN: File name of each floorplan satysfying the filters. In case of inactive floor plan, this string/value pair will be "filename":""
        id11,...,id1M,...,idN1,...,idNM: IDs of up to M anchors binded to the i-th floorplan, i = 1 to N. In case no anchor is binded to floor plan, this string/value pair will be "anchors":[null]
        row11,...,row1M,...,rowN1,...,rowNM: rows of graphics image position of the to M anchors binded to the i-th floorplan, i = 1 to N.
        col11,...,col1M,...,colN1,...,colNM: columns of graphics image position of the to M anchors binded to the i-th floorplan, i = 1 to N.
        binding_status1, ..., binding_statusN: Binding status of anchors set to floorplan, which is one of the following strings: "binding successful", "binding requires at least three anchors" or "binding ill-conditioned". The meaning of binding status is discussed at section 5.4

### 5.2 Floor plans creation

#### Message: ```{"set_floorplan": "create", "filename": filename }``` ####

      This command creates a floorplan composed of a graphics file. "filename" is a mandatory name, whose value is the file name of the graphics file which should already be in the repository.

#### Response: ```{"ack" : {"fp_id": fp_id, "filename": filename}}``` ####

      In case of success, the response sends an "ack" object with the created floorplan ID and filename of the floorplan. These are the basic elements of a floorplan.
      In case of failure, an error message is sent back with the cause of the error. Errors can be caused by graphics file which is not in repository or reaching the maximum number of floorplanes allowed by the system.

### 5.3 Floor plans deletion

#### Message: ```{"set_floorplan": "delete", "fp_id": fp_id }``` ####

      This command deletes a floorplan of ID fp_id.

#### Response: ```{"ack" : {"fp_id": fp_id}}``` ####

      In case of success, the response sends an "ack" object with the deleted floorplan ID.
      In case of failure, an error message is sent back with the cause of the error. Errors can be caused by invalid fp_id.


### 5.4 Floor plans to anchors binding

      In order to allow tag positioning with respect to floorplan graphics file, a minimum number of three anchors should be associated to the foorplan. Anchor to floorplan binding is performed for each anchor.

#### Message: ```{"set_floorplan": "bind", "fp_id": fp_id, "id": id, "row": row, "col": col }``` ####

      The values of mandatory names are:
        fp_id: ID of the floorplan
        id: Beacon ID. The role of such beacon should be "anchor"
        (row,col): Coordinates of anchor at the floorplan graphics file 

#### Response: ```{"ack" : {"fp_id": fp_id, "id": id}}``` ####

      In case of success, the response sends an "ack" object with the floorplan ID and beacon ID. If an anchor is already binded to the floorplan, its coordinates are just updated to (row,col).
      In case of failure, an error message is sent back with the cause of the error. Errors can be caused by invalid values for fp_id and id, beacon which is not an anchor ou (row,col) outside graphics file dimension. These are few examples:
        {"error" : "Missing mandatory JSON name/value pair (cannot find 'fp_id' name string)"}
        {"error" : "Missing mandatory JSON name/value pair (cannot find 'id' name string)"}
        {"error" : "Missing mandatory JSON name/value pair (cannot find 'row' name string)"}
        {"error" : "Missing mandatory JSON name/value pair (cannot find 'col' name string)"}
        {"error" : "Cannot bind anchor to floor plan (floor plan 8 is not active)"}
        {"error" : "Value invalid (fp_id value should be between 0 and 9)"}
        {"error" : "Value invalid (id value should be greater or equal to 16)"}
        {"error" : "Value invalid (row value should be between 1 and 5008)"}
        {"error" : "Value invalid (col value should be between 1 and 8013)"}
        {"error" : "Cannot bind anchor to floor plan (Binding to non anchor)"}

### 5.5 Floor plans to anchors unbinding

      Unbinding anchors may be necessary for many reasons. For instance, for replacing a defectuous anchor or when the user believes that an anchor is not relevant in reducing the error of positioning of tags in graphics file. It should be pointed out that the coordinates of all anchors associated to a floorplan are used to compute the position of a tag on that floorplan. Thus, the positiong errors of the anchors are propagated to the positiong of the tags.

#### Message: ```{"set_floorplan": "unbind", "fp_id": fp_id, "id": id }``` ####

      The values of mandatory names are:
        fp_id: ID of the floorplan
        id: Beacon ID. The role of such beacon should be "anchor"

#### Response: ```{"ack" : {"fp_id": fp_id, "id": id}}``` ####

      In case of success, the response sends an "ack" object with the floorplan ID and beacon ID.
      In case of failure, an error message is sent back with the cause of the error. Errors can be caused by invalid values for fp_id and id, beacon which is not an anchor ou (row,col) outside graphics file dimension.


### 5.6 Graphics files repository listing

#### Message: ```{"get_graphics": "list", "filename": filename }``` ####

      This command gathers informations on all floorplan files uploaded at LS-LYNX server.
    
      This command may have filters. These filters are name/value pairs after the first command pair. Beacon information sent as response are only of beacons satisfying all filters. Filters are optional. If no filter is defined in the message, the requested information of all beacons is sent to the client.
      The defined filters are:
        "filename" (optional), the response of the request will be composed of the floorplan with a given filename.
    
      If there is no floorplan in the system satisfying the filters, the response is {"list" : [null]}


#### Response: ```{"list" : [{"filename": filename1, "filesize": filesize1}, ...., {"filename": filenameN, "filesize": filesizeN}]}``` ####

      The response is organized as an array of JSON objects. For the i-th floorplan satisfying the filters, an array objects is presented, where:
        filename1, ..., filenameN: File name of each graphics file
        filesize1, ..., filesizeN: File size in bytes of each graphics file
    
      If there is no graphics file in the system satisfying the filters, the response is {"file" : [null]}

### 5.7 Graphics files deletion

#### Message: ```{"set_graphics": "delete", "filename": filename }```

```
  This command deletes a graphics file whose name in local repository is filename 
```

#### Response: ```{"ack" : {"filename": filename}}```

```
  In case of success, the response sends an "ack" object with the deleted graphics file name.
  In case of failure, an error message is sent back with the cause of the error. Errors can be caused by invalid filename, the file is being used by a floorplan or even by operating system errors.
```

### 5.8 Download of floorplan graphics files

      This command gathers floorplan graphic files uploaded at LS-LYNX server.

#### Message: ```{"get_graphics": "file", "filename": filename }``` ####

      This message returns the graphics files of the LS-LYNX repository satisfying the filters.
      The defined filters are:
        "filename" (optional), the response of the request will be composed of the graphics with a given filename.

#### Response: ```{"file" : [{"filename": filename1, "filesize": filesize1, "data" : base64data1},{"filename": filenameN, "filesize": filesizeN, "data" : base64dataN}]}``` ####

      The response is organized as an array of JSON objects. For the i-th floorplan satisfying the filters, an array objects is presented, where:
        filename1, ..., filenameN: File name of each graphics file
        filesize1, ..., filesizeN: File size in bytes of each graphics file
        base64data1, ..., base64dataN: Contents of each graphics file encoded as a Base64 string
    
      If there is no graphics file in the system satisfying the filters, the response is {"file" : [null]}


### 5.9 Upload of floorplan graphics files

      This command allows to upload floorplan graphic files at LS-LYNX server.

#### Message: ```{"set_graphics": "file", "filename": filename, "data" : base64data }``` ####

      This message returns the graphics files of all floorplans satisfying the filters informations on all floorplan files uploaded at LS-LYNX server.
      The values of mandatory names are:
        filename: File name of the graphics file
        base64data: Contents of each graphics file encoded as a Base64 string

#### Response: ```{"ack" : {"filename": filename, "filesize": filesize}}``` ####

      In case of success, the response sends back the filename and its size filesize in bytes.
      In case of failure, an error message is sent back with the cause of the error. Errors can be caused by sending an unrecognized graphics file, sending a graphics file with the same filename of another graphics file currently in use, or missing filename and data fields.



## 6. Error reporting
