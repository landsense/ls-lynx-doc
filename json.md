#JSON protocol used for command exchange with the LS-LYNX server via socket TCP/IP#

##1. Introduction##

LS-LYNX server performs beacon mapping and localization, system maintenance and also acts as an external word gateway.
Interfaces with external world are currently implemented using industry-standard TCP/IP sockets.

The server is continuously monitoring the communication channels (TCP/IP). Once a connection is stablished, the server processes requests according to LS-LYNX JSON protocol described in this document. 
If, for some connection, no messages are exchanged for more than 5s, the server closes the connection. This connection timeout policy allows to deal with lost connections.

After a first time connection, the user should set the operating mode using ```set_mode```. At any time the user can change the operating mode. The internal state of each mode is continuously updated by LS-LYNX server. Thus, ```set_mode``` define the current mode, and the next JSON commands will gather informations from that mode. For instance, a call to ```get_beacon``` returns informations related to real beacons (real mode) or to simulated beacons (simulated mode). 

```get_info``` messages allows the user to have accest to internal variables and parameters of the system, according to current mode. 

Beacons one of three possible roles: "none", "tag" and "anchor". The roles are stablished during system deployment and configuration.

Floorplans are graphics files uploaded to LS-LYNX. For this version, only floorplan downloading is allowed. Floorplan management is a more complex task which should be done with system deployment and configuration. Future versions will allow deployment and configuration.

In the sequel, JSON protocol messages are described. 

##2. System information messages##

### 2.1 get_info

#### Message: ```{"get_info": info}``` ####

    info is a string which can be "mode" or "status"

#### Response: ```{info: resp}``` ####

    resp is an integer, according to requested info.
    If info is "status", resp encodes systems status, as below:
        "tracking": System operating normaly, tracking tags
        "mapping" : System is performing mapping. In this case, no tags can be tracked. Anchors positioning is being performed.
    If info is "mode", resp encodes system mode, as below:
        "simulation": Simulation mode
        "real": Real mode.

##3. Operating modes

### 3.1 set_mode

      This command allows the client to set the operating mode of LS-LYNX. 

#### Message: ```{"set_mode": mode}``` ####

      mode is a string, as below:
        "simulation": Simulation mode
        "real": Real mode.

#### Response: ```{"mode": mode}``` ####
      
      mode is as below:
        "simulation": Simulation mode
        "real": Real mode.


##4. Beacons

### 4.1 Listing

      This command allows the client to receive the list of beacons according to the selected filters.

#### Message: ```{"get_beacon": "list", "status" : status, "role": role, "id": [id1, id2, ..., idN]}``` ####

      This command may have filters. These filters are name/value pairs after the first command pair. Beacon information sent as response are only of beacons satisfying all filters. Filters are optional. If no filter is defined in the message, the requested information of all beacons is sent to the client.
      The defined filters are:
        "status" (optional), the response of the request will me composed of all beacons with a given status, whose value may be "active" or "inactive".
        "role" (optional), the response of the request will me composed of all beacons with a given role, whose value may be "tag", "anchor" or "none".
        "id" (optional), is a filter specifying the IDs of the beacons, which can be an array (for more than one ID) or a number (for an specific ID).


#### Response: ```{"list": [{"id":id1,"role",role1,"status",status1},...,{"id":idN,"role",roleN,"status",statusN}]}``` ####

      The response is organized as an array of JSON objects. For the i-th beacons satisfying the filters, an array objects is presented, where:
        id1, ..., idN: ID's of each beacon satysfying the filters
        role1, ..., roleN: status of each beacon satysfying the filters
        status1, ..., statusN: status of each beacon satysfying the filters

      If there is no beacon in the system satisfying the filters, the response is {"list" : [null]}


### 4.2 3D Position at XYZ space

      This command allows the client to receive the 3D positioning of beacons, which may be anchors or tags, according to the selected filters. Only active beacons have their positions computed.

#### Message: ```{"get_beacon": "position_xyz", "status" : status, "role": role, "id": [id1, id2, ..., idN]}``` ####

      This command may have filters. These filters are name/value pairs after the first command pair. Beacon information sent as response are only of beacons satisfying all filters. Filters are optional. If no filter is defined in the message, the requested information of all beacons is sent to the client.
      The defined filters are:
        "status" (optional), the response of the request will me composed of all beacons with a given status, whose value may be "active" or "inactive".
        "role" (optional), the response of the request will me composed of all beacons with a given role, whose value may be "tag", "anchor" or "none".
        "id" (optional), is a filter specifying the IDs of the beacons, which can be an array (for more than one ID) or a number (for an specific ID).


#### Response: ```{"position_xyz": [{"id":id1,"x":x1,"y":y1,"z":z1,"sx":sx1,"sy":sy1,"sz":sz1},...,{"id":id1,"x":x1,"y":y1,"z":z1,"sx":sx1,"sy":sy1,"sz":sz1}]}``` ####

      The response is organized as an array of JSON objects. For the i-th beacons satisfying the filters, an array objects is presented, where:
        id1, ..., idN: ID's of each beacon satysfying the filters
        status1, ..., statusN: status of each beacon satysfying the filters
        (x1,y1,z1),..., (xN,yN,zN): coordinates (x,y,z) of the beacon.
        (sx1,sy1,sz1),..., (sxN,syN,szN): 95% confidence interval, centered at x1, y1 and z1 coordinates. For instance, for beacon with id3 there is a 95% confidence that the beacon is inside the region delimited by ([x3-sx3,x3+sx3],[y3-sy3,y3+sy3],[y3-sy3,y3+sy3]) in 3D space.

      If there is no beacon in the system satisfying the filters, the response is {"position_xyz" : [null]}


### 4.3 Position within floorplan coverage area

      This command allows the client to receive the positioning of beacons which are in floorplan coverage area, which may be anchors or tags, according to the selected filters. It should be pointed out that if some beacon is outside the coverage area of a floorplan, it will be not included in the response. Only active beacons have their positions computed on (also) active floorplans.

#### Message: ```{"get_beacon": "position_floorplan", "status" : status, "role": role, "id": [id1, id2, ..., idN], "fp_id": [fp_id1,...,fp_idN]}``` ####

      This command may have filters. These filters are name/value pairs after the first command pair. Beacon information sent as response are only of beacons satisfying all filters. Filters are optional. If no filter is defined in the message, the requested information of all beacons is sent to the client.
      The defined filters are:
        "status" (optional), the response of the request will me composed of all beacons with a given status, whose value may be "active" or "inactive".
        "role" (optional), the response of the request will me composed of all beacons with a given role, whose value may be "tag", "anchor" or "none".
        "id" (optional), is a filter specifying the IDs of the beacons, which can be an array (for more than one ID) or a number (for an specific ID).
        "floorplan" (optional), is a filter specifying the IDs of the floorplans, which can be an array (for more than one ID) or a number (for an specific ID).


#### Response: ```{"position_floorplan": [{"id":id1,"fp_id":fp_id1,"r":r1,"c":c1,"sr":sr1,"sc":sc1},...,{"id":idN,"fp_id":fp_idN,"r":rN,"c":cN,"sr":srN,"sc":scN}]}``` ####

      The response is organized as an array of JSON objects. For the i-th beacons satisfying the filters, an array objects is presented, where:
        id1, ..., idN: ID's of each beacon satysfying the filters
        fp_id1, ..., fp_idN: ID's of the corresponding floorplan on which the beacon is in the coverage area.
        (r1,c1),..., (rN,cN): coordinates (row,column) of the beacon in the corresponding floorplan.
        (sr1,sc1),..., (srN,scN): 95% confidence interval, centered at r1 and c1 coordinates. For instance, for beacon with id3 there is a 95% confidence that the beacon is inside the region delimited by ([r3-sr3,r3+sr3],[c3-sc3,c3+sc3]) in floorplan pixels space.

      It should be noted that a beacon may be in the coverage area of more than one floorplan. Thus, th ID of such a beacon will appear in more than one object in the array.

      If there is no beacon in the system satisfying the filters, the response is {"position_floorplan" : [null]}


##5. Floorplans 

      Each floorplan is composed os a graphics file in formats JPEG, PNG, BMP or TIFF. Floorplans are uploaded to the server using messages. The floorplan filename is specified at uploading message.
	
      A floorplan is active when it has associated beacons defined as anchors. Such floorplans may have beacons being tracked in their coverability area.

### 5.1 Listing

#### Message: ```{"get_floorplan": "list", "status" : status, "filename": filename, "floorplan" : [fp_id1,...,fp_idN] }``` ####

      This command gathers informations on all floorplan files uploaded at LS-LYNX server.

      This command may have filters. These filters are name/value pairs after the first command pair. Beacon information sent as response are only of beacons satisfying all filters. Filters are optional. If no filter is defined in the message, the requested information of all beacons is sent to the client.
      The defined filters are:
        "status" (optional), the response of the request will me composed of all floorplans with a given status, whose value may be "active" or "inactive".
        "filename" (optional), the response of the request will me composed of the floorplan with a given filename.
        "floorplan" (optional), is a filter specifying the IDs of the floorplans, which can be an array (for more than one ID) or a number (for an specific ID).

      If there is no floorplan in the system satisfying the filters, the response is {"list" : [null]}


#### Response: ```{"list" : [{"id":fp_id1, "status": status1, "filename": filename1}, ...., {"id":fp_idN, "status": statusN, "filename": filenameN}]}``` ####

      The response is organized as an array of JSON objects. For the i-th floorplan satisfying the filters, an array objects is presented, where:
        fp_id1, ..., fp_idN: ID's of each floorplan satysfying the filters
        status1, ..., statusN: status of each floorplan satysfying the filters
        filename1, ..., filenameN: File name of each floorplan satysfying the filters

### 5.2 Download of floorplan graphics files

      This command gathers floorplan graphic files uploaded at LS-LYNX server.

#### Message: ```{"get_floorplan": "file", "status" : status, "filename": filename, "floorplan" : [fp_id1,...,fp_idN] }``` ####

      This message returns the graphics files of all floorplans satisfying the filters informations on all floorplan files uploaded at LS-LYNX server.
      floorplan_id: ID of the floorplan file.

#### Response: ```{"file" : [{"id": fp_id1, "filename": filename1, "data" : base64data1},...,{"id": fp_idN, "filename": filenameN, "data" : base64dataN}]}``` ####

      The response is organized as an array of JSON objects. For the i-th floorplan satisfying the filters, an array objects is presented, where:
        fp_id1, ..., fp_idN: ID's of each floorplan satysfying the filters
        filename1, ..., filenameN: File name of each floorplan satysfying the filters
        base64data1, ..., base64dataN: Contexts of each floorplan file encoded as a Base64 string

      If there is no floorplan in the system satisfying the filters, the response is {"file" : [null]}
